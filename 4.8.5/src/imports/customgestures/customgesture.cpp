#include "customgesture.h"
#include <QDebug>
#include <QEvent>

#include <qdeclarative.h>

CustomGesture* CustomGesture::pInst = NULL;

CustomGesture::CustomGesture(QDeclarativeItem *parent):
    QDeclarativeItem(parent)
{
    // By default, QDeclarativeItem does not draw anything. If you subclass
    // QDeclarativeItem to create a visual item, you will need to uncomment the
    // following line:

    grabGesture(Qt::TapGesture);
    grabGesture(Qt::TapAndHoldGesture);
    grabGesture(Qt::PanGesture);
    grabGesture(Qt::PinchGesture);
    grabGesture(Qt::SwipeGesture);

    CustomGesture::pInst = this;

    QObject::connect(&mTimer, SIGNAL(timeout()), this, SLOT(swipeTimeOut()));
    _init();
}

CustomGesture::~CustomGesture()
{
    QObject::disconnect(&mTimer, SIGNAL(timeout()), this, SLOT(swipeTimeOut()));
}

void CustomGesture::_init()
{
    mType = GT_NONE;

    mStartPos.x = 0;
    mStartPos.y = 0;

    mNewPos.x = 0;
    mNewPos.y = 0;

    mEndPos.x = 0;
    mEndPos.y = 0;

    mStartDis = 0;
    mNewDis = 0;

    mIsPinchFirst = false;

    mCenterX = 0;
    mCenterY = 0;

    mIsSwipeByTime = false;
}

void CustomGesture::gestureHandler()
{    
    if(!isEnabled())
    {
        _init();
        return;
    }
    switch(mType)
    {
        case GT_NONE:
            break;
        case GT_START:
            emit start(mStartPos.x, mStartPos.y);
            break;
        case GT_TAP:
            emit tap();
            break;
        case GT_PAN:
        {
            int diffX =  mNewPos.x - mStartPos.x;
            int diffY =  mNewPos.y - mStartPos.y;
            emit pan(diffX, diffY);
            break;
        }
        case GT_PINCH:
        {            
            if(mNewDis == 0 || mStartDis == 0)
            {
                return;
            }

            double scale = ((double)mNewDis * 100) / ((double)(mStartDis) * 100);

            emit pinch(scale, mCenterX, mCenterY, mIsPinchFirst);
            if(mIsPinchFirst)
                mIsPinchFirst = false;

            break;
        }
        case GT_SWIPE:
        {
            int diffX = mEndPos.x - mStartPos.x;
            QString direction = "none";

            if(mIsSwipeByTime)
            {
                if(diffX < -(800 * 0.05) )
                {
                    direction = "left";
                }
                else if(diffX > (800 * 0.05) )
                {
                    direction = "right";
                }
            }
            else
            {
                if(diffX < -(800 * 0.5) )
                {
                    direction = "left";
                }
                else if(diffX > (800 * 0.5) )
                {
                    direction = "right";
                }
            }

            emit swipe(direction, diffX);
            break;
        }
        case GT_END:
        {            
            emit end(mEndPos.x, mEndPos.y);
            _init();

            mTimer.stop();
            break;
        }
    }
}

bool CustomGesture::isSwipe()
{
    bool b = true;
    int diffY = mStartPos.y - mEndPos.y;
//            if(qAbs((int)diffY) < 40)
    {

    }

    return b;
}

bool CustomGesture::isPinch(int dist)
{
    bool b = false;

    if(dist > 0)
    {
        b = true;
    }

    return b;
}

bool CustomGesture::sceneEvent(QEvent *event)
{
//    qDebug() <<"sceneEvent : "<< event->type();

    return QDeclarativeItem::sceneEvent(event);
}

//bool CustomGesture::event(QEvent *event)
//{
//   qDebug() <<"event : "<< event->type();
//   return QDeclarativeItem::event(event);
//}

void CustomGesture::swipeTimeOut()
{
    mIsSwipeByTime = false;
    mTimer.stop();
}
