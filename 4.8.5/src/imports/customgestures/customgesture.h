#ifndef CUSTOMGESTURE_H
#define CUSTOMGESTURE_H

#include <QDeclarativeItem>
#include <QTimer>

typedef enum{
    GT_NONE = 0,
    GT_START,
    GT_TAP,
    GT_TAP_AND_HOLD,
    GT_PAN,
    GT_SWIPE,
    GT_PINCH,
    GT_END,

}GESTURE_TYPE_E;

typedef struct{
    int x;
    int y;
}GESTURE_POSTION_T;

class CustomGesture : public QDeclarativeItem
{
    Q_OBJECT
    Q_DISABLE_COPY(CustomGesture)

private:
  GESTURE_TYPE_E        mType;

  GESTURE_POSTION_T     mStartPos;
  GESTURE_POSTION_T     mNewPos;
  GESTURE_POSTION_T     mEndPos;

  int                   mStartDis;
  int                   mNewDis;  

  int                   mCenterX;
  int                   mCenterY;
  bool                  mIsPinchFirst;

  bool                  mIsSwipeByTime;

public:
                        CustomGesture(QDeclarativeItem *parent = 0);
                        ~CustomGesture();

    void                _init();

    void                setStartPos(int x, int y){mStartPos.x = x; mStartPos.y = y;}
    GESTURE_POSTION_T   getStartPos(){return mStartPos;}

    void                setNewPos(int x, int y){mNewPos.x = x; mNewPos.y = y;}
    GESTURE_POSTION_T   getNewPos(){return mNewPos;}

    void                setEndPos(int x, int y){mEndPos.x = x; mEndPos.y = y;}
    GESTURE_POSTION_T   getEndPos(){return mEndPos;}

    void                setStartDis(int dis){mStartDis = dis; mIsPinchFirst = true;}
    int                 getStartDis(){return mStartDis;}

    void                setNewDis(int dis){mNewDis = dis;}
    int                 getNewDis(){return mNewDis;}

    void                setCenterPos(int x, int y){mCenterX = x, mCenterY = y;}

    void                gestureHandler();
    void                setGestureType(GESTURE_TYPE_E type){mType = type;}
    GESTURE_TYPE_E      getGestureType(){return mType;}

    bool                isSwipe();
    bool                isPinch(int dist);    
    bool                isPinchFirst(){return mIsPinchFirst;}

    void                setIsSwipeByTime(bool isSwipe){mIsSwipeByTime = isSwipe;}

//    QTimer              getTimer(){return mTimer;}

    QTimer              mTimer;
    static              CustomGesture*   pInst;

protected:
    bool                sceneEvent(QEvent *event);
//    bool                event(QEvent *event);

signals:
  void                  start(int startX, int startY);
  void                  pan(int diffX, int diffY);
  void                  pinch(double scale, int centerX, int centerY, bool isFirst);
  void                  swipe(QString direction, int diffX);
  void                  end(int endX, int endY);

  void                  tap(void);
  void                  tapAndHold(void);

public slots:
  void                  swipeTimeOut();

};

QML_DECLARE_TYPE(CustomGesture)

#endif // CUSTOMGESTURE_H

