#ifndef CUSTOMGESTURERECOGNIZER_H
#define CUSTOMGESTURERECOGNIZER_H

#include "qgesturerecognizer.h"

class QCustomGestureRecognizer : public QGestureRecognizer
{
public:
    QCustomGestureRecognizer();

    QGesture *create(QObject *target);
    QGestureRecognizer::Result recognize(QGesture *state, QObject *watched, QEvent *event);
    void reset(QGesture *state);
};

#endif // CUSTOMGESTURERECOGNIZER_H
