TARGETPATH = Qt/labs/customgestures

TEMPLATE = lib
TARGET = customgestures
QT += declarative
CONFIG += qt plugin

TARGET = $$qtLibraryTarget($$TARGET)

# Input
SOURCES += \
    customgestures_plugin.cpp \
    customgesture.cpp \
    customgesturerecognizer.cpp

HEADERS += \
    customgestures_plugin.h \
    customgesturerecognizer.h \
    customgesture.h

OTHER_FILES = qmldir

QTDIR_build:DESTDIR = $$QT_BUILD_TREE/imports/$$TARGETPATH
target.path = $$[QT_INSTALL_IMPORTS]/$$TARGETPATH

qmldir.files += $$PWD/qmldir
qmldir.path +=  $$[QT_INSTALL_IMPORTS]/$$TARGETPATH

symbian:{
    TARGET.UID3 = 0x2002131F
    TARGET.EPOCALLOWDLLDATA = 1
    include($$QT_SOURCE_TREE/demos/symbianpkgrules.pri)

    importFiles.sources = $$DESTDIR/qmlgestureareaplugin$${QT_LIBINFIX}.dll qmldir
    importFiles.path = $$QT_IMPORTS_BASE_DIR/$$TARGETPATH

    DEPLOYMENT = importFiles
}

