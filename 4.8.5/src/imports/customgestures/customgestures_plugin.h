#ifndef CUSTOMGESTURES_PLUGIN_H
#define CUSTOMGESTURES_PLUGIN_H

#include <QDeclarativeExtensionPlugin>

class CustomgesturesPlugin : public QDeclarativeExtensionPlugin
{
    Q_OBJECT

public:
    void registerTypes(const char *uri);
};

#endif // CUSTOMGESTURES_PLUGIN_H

