#include "customgestures_plugin.h"
#include "customgesture.h"
#include "customgesturerecognizer.h"

#include <QtGui/qgesture.h>

#include <qdeclarative.h>

void CustomgesturesPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(QLatin1String(uri) == QLatin1String("Qt.labs.customgestures"));

    qmlRegisterType<CustomGesture>(uri, 1, 0, "CustomGesture");

//    qmlRegisterCustomType<QDeclarativeCustomGestureArea>(uri,1,0, "CustomGesture", new QDeclarativeCustomGestureAreaParser);

    QGestureRecognizer::unregisterRecognizer(Qt::TapGesture);
//    QGestureRecognizer::unregisterRecognizer(Qt::TapAndHoldGesture);
//    QGestureRecognizer::unregisterRecognizer(Qt::PanGesture);
//    QGestureRecognizer::unregisterRecognizer(Qt::PinchGesture);
//    QGestureRecognizer::unregisterRecognizer(Qt::SwipeGesture);

    QGestureRecognizer::registerRecognizer(new QCustomGestureRecognizer());
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(Customgestures, CustomgesturesPlugin)
#endif

