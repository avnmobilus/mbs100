#include "qgesture.h"
#include "qevent.h"
#include "qwidget.h"
#include "customgesturerecognizer.h"
#include <QDebug>
#include <private/qevent_p.h>
#include "customgesture.h"

QCustomGestureRecognizer::QCustomGestureRecognizer()
{
    qDebug() << "QTapGestureRecognizer";
}

QGesture *QCustomGestureRecognizer::create(QObject *target)
{
//    qDebug() << "create";
    if (target) {
        if (target->isWidgetType())
            static_cast<QWidget *>(target)->setAttribute(Qt::WA_AcceptTouchEvents);
        else
            return 0;
    }

    return new QTapGesture;
}

QGestureRecognizer::Result QCustomGestureRecognizer::recognize(QGesture *state,
                                                            QObject *,
                                                            QEvent *event)
{
    const QNativeGestureEvent *ev = static_cast<const QNativeGestureEvent *>(event);

    Result result = QGestureRecognizer::Ignore;

    if(event->type() == QEvent::NativeGesture)
    {
//        qDebug() << "NativeGesture : ";

//        qDebug() << "ev->position.x() : " << ev->position.x();
//        qDebug() << "ev->position.y() : " << ev->position.y();
//          qDebug() << "ev->argument : " << ev->argument;
//          qDebug() << "ev->argument1111 : " << ev->argument / 1000;
//        qDebug() << "ev->position.y() : " << ev->percentage;



//        qDebug() << "ev->gestureType : " << ev->gestureType;

        if(ev->gestureType == QNativeGestureEvent::GestureBegin)
        {
//            qDebug() << "GestureBegin";

            CustomGesture::pInst->setGestureType(GT_START);
            CustomGesture::pInst->setStartPos(ev->position.x(),ev->position.y());

            result = QGestureRecognizer::MayBeGesture;
            CustomGesture::pInst->gestureHandler();

            CustomGesture::pInst->setIsSwipeByTime(true);
            CustomGesture::pInst->mTimer.start(700);
        }        

        if(ev->gestureType == QNativeGestureEvent::Pan)
        {
            CustomGesture::pInst->setNewPos(ev->position.x(),ev->position.y());

            if(CustomGesture::pInst->getGestureType() == GT_START)
            {                
                if(CustomGesture::pInst->isPinch(ev->argument >> 32))
                {
                    CustomGesture::pInst->setStartDis(ev->argument >> 32);
                    CustomGesture::pInst->setGestureType(GT_PINCH);
                }
                else
                {
                    CustomGesture::pInst->setGestureType(GT_PAN);
                }

            }
            else if(CustomGesture::pInst->getGestureType() == GT_PAN)
            {
                CustomGesture::pInst->setNewPos(ev->position.x(),ev->position.y());
            }
            else if(CustomGesture::pInst->getGestureType() == GT_PINCH)
            {
                if(CustomGesture::pInst->isPinchFirst())
                    CustomGesture::pInst->setCenterPos(ev->position.x(),ev->position.y());

                CustomGesture::pInst->setNewDis(ev->argument >> 32);
            }

            result = QGestureRecognizer::TriggerGesture;
            CustomGesture::pInst->gestureHandler();

        }

        if(ev->gestureType == QNativeGestureEvent::GestureEnd)
        {
//            qDebug() << "GestureEnd";

            CustomGesture::pInst->setEndPos(ev->position.x(),ev->position.y());

            if(CustomGesture::pInst->getGestureType() == GT_START)
            {
                CustomGesture::pInst->setGestureType(GT_TAP);
                CustomGesture::pInst->gestureHandler();
            }
            else if(CustomGesture::pInst->getGestureType() == GT_PAN)
            {
                if(CustomGesture::pInst->isSwipe())
                {
                    CustomGesture::pInst->setGestureType(GT_SWIPE);
                    CustomGesture::pInst->gestureHandler();
                }

            }

            CustomGesture::pInst->setGestureType(GT_END);
            CustomGesture::pInst->gestureHandler();
            result = QGestureRecognizer::FinishGesture;
        }

    }

    return result;

}

void QCustomGestureRecognizer::reset(QGesture *state)
{
    if (!state)
        return;
//    QTapGesture *q = static_cast<QTapGesture *>(state);

    QGestureRecognizer::reset(state);
}

//
// QTapAndHoldGestureRecognizer
//
