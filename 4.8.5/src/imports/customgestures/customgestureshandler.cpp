#include "customgestureshandler.h"
#include "qde"

class QDeclarativeTapGestureHandler : public QDeclarativeGestureHandler
{
    Q_OBJECT
public:
    QDeclarativeTapGestureHandler(QObject *parent = 0)
        : QDeclarativeGestureHandler(parent)
    {
        setGestureType(Qt::TapGesture);
    }

private:
    Q_DISABLE_COPY(QDeclarativeTapGestureHandler)
};
