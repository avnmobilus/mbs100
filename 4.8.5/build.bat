@echo on
echo Setting up a qt environment...

call "C:\Program Files (x86)\Microsoft Visual Studio 9.0\Common7\Tools\vsvars32.bat"


configure -platform win32-msvc2008 -xplatform wince70embedded-armv4i-msvc2008 -opengl es2 -openvg -nomake demos -nomake examples

set PATH=C:\Program Files (x86)\Windows CE Tools\SDKs\TCC8930_MOVI_SDK_ARMV7\Bin;%PATH%
set INCLUDE=C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\ce\include;C:\Program Files (x86)\Windows CE Tools\SDKs\TCC8930_MOVI_SDK_ARMV7\include\ARMv7;C:\Program Files (x86)\Windows CE Tools\SDKs\TCC8930_MOVI_SDK_ARMV7\include;C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\ce\atlmfc\include;C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\SmartDevices\SDK\SQL Server\Mobile\v3.0	   
set LIB=C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\ce\atlmfc\lib\armv4i;C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\ce\lib\armv4i;C:\Program Files (x86)\Windows CE Tools\SDKs\TCC8930_MOVI_SDK_ARMV7\lib\ARMv7;C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\ce\atlmfc\lib\ARMv7;C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\ce\lib\ARMv7

nmake
